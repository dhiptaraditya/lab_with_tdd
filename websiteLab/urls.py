"""websiteLab URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import re_path, include
from beranda import views as beranda_views
from story_10 import views as story_10_views

urlpatterns = [
    re_path(r'^admin/', admin.site.urls),
    re_path(r'^profile/',beranda_views.profile),
    re_path(r'^register/',story_10_views.index),
    re_path(r'^db/',story_10_views.showdb),
    re_path(r'^registrate/',story_10_views.registrate),
    re_path(r'story-11',include('story_11.urls')),
    re_path(r'^deletion/',story_10_views.deletion),
    re_path(r'^', include('beranda.urls')),
    # re_path(r'^beranda/', views.index),
    # re_path(r'^addStatus/', views.addStatus),    
]