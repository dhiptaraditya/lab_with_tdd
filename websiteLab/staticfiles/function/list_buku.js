//St 9
function loadBooks(){
  var xmlhttp = new XMLHttpRequest(),
  method = 'GET',
  url = "/bukus/",
  defsrc = "quilting/";
  console.log(document.getElementById("search").value);
  if(document.getElementById("search").value != "")defsrc = document.getElementById("search").innerHTML;
  url = url + defsrc
  xmlhttp.open(method, url, true);
  xmlhttp.onload = function () {
      var json = this.response;
      var datas = JSON.parse(json);
      // console.log(datas);
      var ctr = 0;
      document.getElementById("tabel").innerHTML = "";
      for (i = 0; i < datas.datas.length;i++) {
        var data = datas.datas[i];
        var node = document.createElement("tr");

        var previewNode = document.createElement("th");
        var previewPic = document.createElement("IMG");
        previewPic.setAttribute("src", data.preview);
        previewNode.appendChild(previewPic);
        
        var title = document.createTextNode(data.title);
        var titleWrite = document.createElement("h3");
        var titleNode = document.createElement("td");
        titleWrite.appendChild(title);
        titleNode.appendChild(titleWrite);

        // console.log(data.author);
        var author = document.createTextNode(data.author);
        var authorWrite = document.createElement("h3");
        var authorNode = document.createElement("td");
        authorWrite.appendChild(author);
        authorNode.appendChild(authorWrite);

        var favoWrite = document.createElement("h2");
        var favoCont = document.createElement("button")
        var favoNode = document.createElement("td");
        favoWrite.classList.add("glyphicon");
        if(data.is_favo)favoWrite.classList.add("glyphicon-star");
        else favoWrite.classList.add("glyphicon-star-empty");
        favoCont.classList.add('btn');
        favoCont.setAttribute("id", i);
        // favoCont.addEventListener("click", ()=>lol(id));
        // favoCont.setAttribute("onclick", ()=>lol(i));

        favoCont.appendChild(favoWrite);
        favoNode.appendChild(favoCont);


        node.appendChild(previewNode);  
        node.appendChild(titleNode);
        node.appendChild(authorNode);
        node.appendChild(favoNode);

        document.getElementById("tabel").appendChild(node);
        document.getElementById(i).onclick = () => lol();
      }
  };
  xmlhttp.send();
}

//Challenge St 9
function loadBooksReady(){
  var xmlhttp = new XMLHttpRequest(),
  method = 'GET',
  url = "/bukus/";
  console.log(document.getElementById("search").value);
  defsrc = document.getElementById("search").value;
  url = url + defsrc +'/'
  xmlhttp.open(method, url, true);
  xmlhttp.onload = function () {
      // console.log(xhr.responseText);
      var json = this.response;
      var datas = JSON.parse(json);

      document.getElementById("tabel").innerHTML = "";
      for (i = 0; i < datas.datas.length;i++) {
        var data = datas.datas[i];
        var node = document.createElement("tr");

        var previewNode = document.createElement("th");
        var previewPic = document.createElement("IMG");
        previewPic.setAttribute("src", data.preview);
        previewNode.appendChild(previewPic);
        
        var title = document.createTextNode(data.title);
        var titleWrite = document.createElement("h3");
        var titleNode = document.createElement("td");
        titleWrite.appendChild(title);
        titleNode.appendChild(titleWrite);

        // console.log(data.author);
        var author = document.createTextNode(data.author);
        var authorWrite = document.createElement("h3");
        var authorNode = document.createElement("td");
        authorWrite.appendChild(author);
        authorNode.appendChild(authorWrite);

        var favoWrite = document.createElement("h2");
        var favoCont = document.createElement("button")
        var favoNode = document.createElement("td");
        favoWrite.classList.add("glyphicon");
        if(data.is_favo)favoWrite.classList.add("glyphicon-star");
        else favoWrite.classList.add("glyphicon-star-empty");
        favoCont.classList.add('btn');
        favoCont.setAttribute("id", i);
        // favoCont.addEventListener("click", ()=>lol(id));
        // favoCont.setAttribute("onclick", ()=>lol(i));

        favoCont.appendChild(favoWrite);
        favoNode.appendChild(favoCont);


        node.appendChild(previewNode);  
        node.appendChild(titleNode);
        node.appendChild(authorNode);
        node.appendChild(favoNode);

        document.getElementById("tabel").appendChild(node);
        document.getElementById(i).onclick = () => lol();
      }
  };
  xmlhttp.send();
}
var nFavorit = 0;
function lol(){
  // console.log(event.currentTarget.children[0].classList.contains("glyphicon-star-empty"));
  if(event.currentTarget.children[0].classList.contains("glyphicon-star-empty")){
    nFavorit++;
    event.currentTarget.children[0].classList.remove("glyphicon-star-empty");
    event.currentTarget.children[0].classList.add("glyphicon-star");
  }else{
    nFavorit--;
    event.currentTarget.children[0].classList.remove("glyphicon-star");
    event.currentTarget.children[0].classList.add("glyphicon-star-empty");
  }
  // console.log(nFavorit);
  if(nFavorit>0){
    var titleWrite = document.createElement("h3");
    titleWrite.innerHTML = "Number Of Favorit :\t" + nFavorit;
    // console.log(titleWrite);
    document.getElementById("numberOfFavorit").innerHTML = "";
    document.getElementById("numberOfFavorit").appendChild(titleWrite);
    document.getElementById("numberOfFavorit").classList.add("jumbotron");

  }else{
    document.getElementById("numberOfFavorit").innerHTML = "";    
    document.getElementById("numberOfFavorit").classList.remove("jumbotron");
  }
}