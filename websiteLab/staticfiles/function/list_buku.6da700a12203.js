function loadBooks(){
  var xmlhttp = new XMLHttpRequest(),
  method = 'GET',
  url = "/bukus/";

  xmlhttp.open(method, url, true);
  xmlhttp.onload = function () {
      var json = this.response;
      var datas = JSON.parse(json);
      var ctr = 0;
      document.getElementById("tabel").innerHTML = "";
      for (i = 0; i < datas.datas.length;i++) {
        var data = datas.datas[i];
        var node = document.createElement("tr");

        var previewNode = document.createElement("th");
        var previewPic = document.createElement("IMG");
        previewPic.setAttribute("src", data.preview);
        previewNode.appendChild(previewPic);
        
        var title = document.createTextNode(data.title);
        var titleWrite = document.createElement("h3");
        var titleNode = document.createElement("td");
        titleWrite.appendChild(title);
        titleNode.appendChild(titleWrite);

        var publisher = document.createTextNode(data.publisher);
        var publisherWrite = document.createElement("h3");
        var publisherNode = document.createElement("td");
        publisherWrite.appendChild(publisher);
        publisherNode.appendChild(publisherWrite);

        var favoWrite = document.createElement("h2");
        var favoCont = document.createElement("button")
        var favoNode = document.createElement("td");
        favoWrite.classList.add("glyphicon");
        if(data.is_favo)favoWrite.classList.add("glyphicon-star");
        else favoWrite.classList.add("glyphicon-star-empty");
        favoCont.classList.add('btn');
        favoCont.setAttribute("id", i);
        // favoCont.addEventListener("click", ()=>lol(id));
        // favoCont.setAttribute("onclick", ()=>lol(i));

        favoCont.appendChild(favoWrite);
        favoNode.appendChild(favoCont);


        node.appendChild(previewNode);
        node.appendChild(titleNode);
        node.appendChild(publisherNode);
        node.appendChild(favoNode);

        document.getElementById("tabel").appendChild(node);
        document.getElementById(i).onclick = () => lol();
      }
  };
  xmlhttp.send();
}

var nFavorit = 0;
function lol(){
  // console.log(event.currentTarget.children[0].classList.contains("glyphicon-star-empty"));
  if(event.currentTarget.children[0].classList.contains("glyphicon-star-empty")){
    nFavorit++;
    event.currentTarget.children[0].classList.remove("glyphicon-star-empty");
    event.currentTarget.children[0].classList.add("glyphicon-star");
  }else{
    nFavorit--;
    event.currentTarget.children[0].classList.remove("glyphicon-star");
    event.currentTarget.children[0].classList.add("glyphicon-star-empty");
  }
  console.log(nFavorit);
  if(nFavorit>0){
    var titleWrite = document.createElement("h3");
    titleWrite.innerHTML = nFavorit;
    document.getElementById("numberOfFavorit").innerHTML = "";
    document.getElementById("numberOfFavorit").appendChild(titleWrite);

  }else{
    document.getElementById("numberOfFavorit").innerHTML = "";    
  }
}