$(document).ready(function){ 
  $(function(){
    $("#accordion").accordion({
      collapsible: true,
      active:true,
      heighStyle:'content',
      icons: { "header": "ui-icon-plus", "activeHeader": "ui-icon-minus" }
    });
  });
}
