// Story 10
var name_ok = false;
var email_ok = false;
var pass_ok = false;
function validateUsernameExist(){
  var name = document.getElementById("username").value;
  var xmlhttp = new XMLHttpRequest(),
  method = 'GET',
  url = "/db/";
  xmlhttp.open(method, url, true);
  xmlhttp.onload = function(){
    var json = this.response;
    var people = JSON.parse(json).data;
    // console.log(people.length);
    var ada = false;
    for(i = 0; i<people.length; i++){
      // console.log(people[i].name);
      if(people[i].name === name)ada = true;
    }
    if(ada ===  true){
      document.getElementById("user_availability").innerHTML = "already exist";
      name_ok = false;
    }else{
      document.getElementById("user_availability").innerHTML = "You can use this user";
      name_ok = true;
    }
    if(email_ok & name_ok & pass_ok)document.getElementById("registrate").disabled = false;
    console.log(email_ok & name_ok & pass_ok);
  };
  xmlhttp.send(); 
}


function validateEmailExist(){
  var email = document.getElementById("email").value;
  var xmlhttp = new XMLHttpRequest(),
  method = 'GET',
  url = "/db/";
  xmlhttp.open(method, url, true);
  var valid = true;
  xmlhttp.onload = function(){
    var json = this.response;
    var people = JSON.parse(json).data;
    var ada = false;
    var array = email.split("@");
    if(array.length > 1){
      var array2 = array[1].split('.');
      if(array2.length !== 2)valid = false;
      else if(array2[1]!=="com") valid = false;
    }else valid = false;
    if(valid === false){
      email_ok = false;
      document.getElementById("email_availability").innerHTML = "please input correct email";
    }else{      
      for(i = 0; i<people.length; i++){
        if(people[i].email === email)ada = true;
      }
      if(ada ===  true){
        document.getElementById("email_availability").innerHTML = "email already used";
        email_ok = false;
      }else{
        document.getElementById("email_availability").innerHTML = "email available";
        email_ok = true;
      }
    }
    if(email_ok & name_ok & pass_ok)document.getElementById("registrate").disabled = false;
    console.log(email_ok & name_ok & pass_ok);

  };
  xmlhttp.send(); 
}

function validatePassword(){
  var pass = document.getElementById("password").value;
  var xmlhttp = new XMLHttpRequest(),
  method = 'GET',
  url = "/db/";
  xmlhttp.open(method, url, true);
  xmlhttp.onload = function(){
    var json = this.response;
    var people = JSON.parse(json).data;
    // console.log(pass);
    var ada = false;
    for(i = 0; i<people.length; i++){
      // console.log(people[i].password);
      if(people[i].password === pass)ada = true;
    }
    if(pass.length <= 8){
      document.getElementById("pass_strength").innerHTML = "Password needs to be 8 character(s) or more";
      pass_ok = false;
    }else
    if(ada ===  true){
      document.getElementById("pass_strength").innerHTML = "Password weak";
      pass_ok = false;
    }else{
      document.getElementById("pass_strength").innerHTML = "Password strong";
      pass_ok = true;
    }
    if(email_ok & name_ok & pass_ok)document.getElementById("registrate").disabled = false;
    console.log(email_ok & name_ok & pass_ok);

  };
  xmlhttp.send();
}

function refresh(){
  document.getElementById("password").value = "";
  document.getElementById("email").value = "";
  document.getElementById("username").value = "";
  document.getElementById("registrate").disabled = true;  
  name_ok = false;
  pass_ok = false;
  email_ok = false;
}

function registrate(){
  var csrf = document.getElementsByTagName("input")[0].value;
  console.log(csrf);
  var xmlhttp = new XMLHttpRequest(),
  method = "POST",
  url = "/registrate/";


  xmlhttp.open(method, url, true);
  xmlhttp.setRequestHeader('X-CSRFToken', csrf);      
  xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

  var pass = document.getElementById("password").value;
  var username = document.getElementById("username").value;
  var email = document.getElementById("email").value;

  console.log(csrf);
  console.log(pass_ok && email_ok && name_ok);
  xmlhttp.onload = function(){
    if (pass_ok && email_ok && name_ok){
      refresh();
      alert(username + "terdaftar");
      console.log(JSON.stringify({name:username, email:email, pass:pass}))
    }else{
      if(name_ok === false){
        alert("Your username is not okay")
      }else
      if(email_ok === false){
        alert("Your email is not okay")
      }else
      if(pass_ok === false){
        alert("Your password is not okay")
      }
    }
  };
  xmlhttp.send(JSON.stringify({"name":username, "email":email, "pass":pass}));


  var xmlhttp = new XMLHttpRequest(),
  method = 'GET',
  url = "/db/";
  xmlhttp.open(method, url, true);
  xmlhttp.onload = function () {
      var json = this.response;
      var people = JSON.parse(json).data;
      console.log(people);
      document.getElementById("tabel").innerHTML = "";
      for (i = 0; i < people.length;i++) {
        var person = people[i];
        var node = document.createElement("tr");
        
        var name = document.createTextNode(person.name);
        var nameWrite = document.createElement("h3");
        var nameNode = document.createElement("td");
        nameWrite.appendChild(name);
        nameNode.appendChild(nameWrite);

        // console.log(data.author);
        var email = document.createTextNode(person.email);
        var emailWrite = document.createElement("h3");
        var emailNode = document.createElement("td");
        emailWrite.appendChild(email);
        emailNode.appendChild(emailWrite);

        var favoWrite = document.createElement("h2");
        var favoCont = document.createElement("button")
        var favoNode = document.createElement("td");

        favoWrite.innerHTML = "Unsubscribe";
        favoCont.classList.add('btn');
        favoCont.setAttribute("id", i);
        favoCont.appendChild(favoWrite);
        favoNode.appendChild(favoCont);


        node.appendChild(nameNode);  
        node.appendChild(emailNode);
        node.appendChild(favoNode);

        document.getElementById("tabel").appendChild(node);
        document.getElementById(i).onclick = () => unsub();
      }
  };
  xmlhttp.send();  
  // xmlhttp.send(JSON.stringify({ "email": "hello@user.com", "response": { "name": "Tester" } }));

}


// Challenge 10
function loadSubscriber(){
  var xmlhttp = new XMLHttpRequest(),
  method = 'GET',
  url = "/db/";
  xmlhttp.open(method, url, true);
  xmlhttp.onload = function () {
      var json = this.response;
      var people = JSON.parse(json).data;
      console.log(people);
      document.getElementById("tabel").innerHTML = "";
      for (i = 0; i < people.length;i++) {
        var person = people[i];
        var node = document.createElement("tr");
        
        var name = document.createTextNode(person.name);
        var nameWrite = document.createElement("h3");
        var nameNode = document.createElement("td");
        nameWrite.appendChild(name);
        nameNode.appendChild(nameWrite);

        // console.log(data.author);
        var email = document.createTextNode(person.email);
        var emailWrite = document.createElement("h3");
        var emailNode = document.createElement("td");
        emailWrite.appendChild(email);
        emailNode.appendChild(emailWrite);

        var favoWrite = document.createElement("h2");
        var favoCont = document.createElement("button")
        var favoNode = document.createElement("td");

        favoWrite.innerHTML = "Unsubscribe";
        favoCont.classList.add('btn');
        favoCont.setAttribute("id", i);
        favoCont.appendChild(favoWrite);
        favoNode.appendChild(favoCont);


        node.appendChild(nameNode);  
        node.appendChild(emailNode);
        node.appendChild(favoNode);

        document.getElementById("tabel").appendChild(node);
        document.getElementById(i).onclick = () => unsub();
      }
  };
  xmlhttp.send();
}

function unsub(){
  var ada = false;
  var txt;
  var passInput = prompt("Please enter the password");

  console.log(event.currentTarget.parentElement.parentElement.children[0].children[0].innerHTML);
  var nama, email;
  var xmlhttp = new XMLHttpRequest(),
  method = 'GET',
  namaIn = event.currentTarget.parentElement.parentElement.children[0].children[0].innerHTML;
  url = "/db/";
  xmlhttp.open(method, url, true);
  xmlhttp.onload = function(){
    var json = this.response;
    var people = JSON.parse(json).data;
    for(i = 0; i<people.length; i++){
      if(people[i].password === passInput && people[i].name === namaIn){
        nama = people[i].name;
        email = people[i].email;
        ada = true;
      }
    }
    // console.log(ada);
    if(ada === false){
      alert("Wrong password for user" + namaIn);
      return;
    }
  };
  xmlhttp.send();

  var xmlhttp = new XMLHttpRequest(),
  method = "POST",
  url = "/deletion/";
  xmlhttp.open(method, url, true);
  xmlhttp.onload = function(){
    // console.log(ada);  
  };
  xmlhttp.send(JSON.stringify({"pass":passInput}));


  var xmlhttp = new XMLHttpRequest(),
  method = 'GET',
  url = "/db/";
  xmlhttp.open(method, url, true);
  xmlhttp.onload = function () {
      var json = this.response;
      var people = JSON.parse(json).data;
      console.log(people);
      document.getElementById("tabel").innerHTML = "";
      for (i = 0; i < people.length;i++) {
        var person = people[i];
        var node = document.createElement("tr");
        
        var name = document.createTextNode(person.name);
        var nameWrite = document.createElement("h3");
        var nameNode = document.createElement("td");
        nameWrite.appendChild(name);
        nameNode.appendChild(nameWrite);

        // console.log(data.author);
        var email = document.createTextNode(person.email);
        var emailWrite = document.createElement("h3");
        var emailNode = document.createElement("td");
        emailWrite.appendChild(email);
        emailNode.appendChild(emailWrite);

        var favoWrite = document.createElement("h2");
        var favoCont = document.createElement("button")
        var favoNode = document.createElement("td");

        favoWrite.innerHTML = "Unsubscribe";
        favoCont.classList.add('btn');
        favoCont.setAttribute("id", i);
        favoCont.appendChild(favoWrite);
        favoNode.appendChild(favoCont);


        node.appendChild(nameNode);  
        node.appendChild(emailNode);
        node.appendChild(favoNode);

        document.getElementById("tabel").appendChild(node);
        document.getElementById(i).onclick = () => unsub();
      }
  };
  xmlhttp.send();
}