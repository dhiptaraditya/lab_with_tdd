from django import forms
from .models import Status

class StatusForms(forms.ModelForm):
	class Meta:
		model = Status

		fields = ('status',)
		widgets = {
			'status' : forms.Textarea(attrs={'class': 'form-control','rows':'5', 'type' : 'text'}),
		}