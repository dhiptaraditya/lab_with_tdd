from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve
from django.utils import timezone
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
from beranda import views



from .models import Status
from .forms import StatusForms
from .views import index, profile, buku


# Create your tests here.
#story
class berandaUnitTest(TestCase):
  def test_url_exist(self):
    response = Client().get('/')
    self.assertEqual(response.status_code, 200)

  def test_function_caller_exist(self):
    found = resolve('/')
    self.assertEqual(found.func, index)

  def test_landingpage_containt(self):
    response = self.client.get('/')
    html_response = response.content.decode('utf8')
    self.assertIn('Hello apa kabar?', html_response)

  def test_can_create_new_status(self):
    testText = 'Lorem Ipsum'
    new_act = Status.objects.create(status= testText, dateTime=timezone.now())
    counting_all_available_status =  Status.objects.all().count();
    self.assertEqual(counting_all_available_status, 1)

  def test_form_Status_rendered(self):
    # form = StatusForm()
    response = Client().get('/')
    html_response = response.content.decode('utf8')
    self.assertIn('status', html_response)

  
  def test_post_success_and_render_the_result(self):
    testText = 'Lorem Ipsum'
    # form_data = {'status':testText}
    response_post = Client().post('/', {'status': testText})
    self.assertEqual(response_post.status_code, 302)
    # tmp =     
    response= Client().get('/')
    html_response = response.content.decode('utf8')
    self.assertIn(testText, html_response)



class bukuUnitTest(TestCase):
  def test_url_exist(self):
    response = Client().get('/buku')
    self.assertEqual(response.status_code, 200)

  def test_function_caller_exist(self):
    found = resolve('/buku')
    self.assertEqual(found.func, buku)




  # def test_lab5_post_error_and_render_the_result(self):
  #   test = 'Anonymous'
  #   response_post = Client().post('/', {'status': '  '})
  #   self.assertEqual(response_post.status_code, 302)

  #   response= Client().get('/')
  #   html_response = response.content.decode('utf8')
  #   self.assertNotIn(test, html_response)

  def test_TextField_300char(self):
    testText = "?" * 301
    response = Client().post("/", {"status": testText})
    # self.asserIsNotNone(stats)
    self.assertEqual(Status.objects.filter(status = testText).count(), 0)



#challenge story 5
class ProfileTestPage(TestCase):
  def test_url_exist(self):
    response = Client().get('/profile/')
    self.assertEqual(response.status_code, 200)

  def test_function_caller_exist(self):
    found = resolve('/profile/')
    self.assertEqual(found.func, profile)

  def test_landingpage_containt(self):
    response = self.client.get('/profile/')
    html_response = response.content.decode('utf8')
    self.assertIn('Activity', html_response)


# class SeleniumFunctionalTest(LiveServerTestCase):
#   def setUp(self):
#     chrome_options = Options()
#         # remove four lines below to test locally
#     chrome_options.add_argument('--dns-prefetch-disable')
#     chrome_options.add_argument('--no-sandbox')
#     chrome_options.add_argument('--headless')
#     chrome_options.add_argument('disable-gpu')
#     self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#     super(SeleniumFunctionalTest, self).setUp()


#   def tearDown(self):
#     self.selenium.quit()
#     super(SeleniumFunctionalTest, self).tearDown()

#   def test_post(self):
#     browser = self.selenium
#     browser.get(self.live_server_url)
#     new_comment = browser.find_element_by_id('id_status')
#     self.assertIn("",browser.page_source)
#     new_comment.send_keys("G Oalah")
#     submit = browser.find_element_by_class_name('save')
#     submit.send_keys(Keys.RETURN) 
#     browser.get(self.live_server_url)
#     time.sleep(5)
#     self.assertIn("G Oalah", browser.page_source)

#   def test_color_jumbotron(self):
#     browser = self.selenium
#     browser.get(self.live_server_url)
#     jumbo = browser.find_element_by_class_name('jumbotron')
#     jumbo_color = jumbo.value_of_css_property("background-color")
#     self.assertEquals("rgba(10, 86, 209, 0.5)", jumbo_color);
  
#   def test_style_h2(self):
#     browser = self.selenium
#     browser.get(self.live_server_url)
#     badan = browser.find_element_by_tag_name("body")
#     gambar_badan = badan.value_of_css_property("background-image")
#     self.assertEquals("url("+ "\"" +"https://hdqwalls.com/download/calm-ocean-water-5k-jd-2048x1152.jpg"+ "\"" +")", gambar_badan);

#   def test_header_utama(self):
#     browser = self.selenium
#     browser.get(self.live_server_url)
#     self.assertTrue(browser.find_elements_by_css_selector('body > h1')[0].is_displayed())

#   def test_random_tag(self):
#     browser = self.selenium
#     browser.get(self.live_server_url)
#     self.assertFalse(browser.find_elements_by_css_selector('div.row > div.col-6 > div') == None) 


# class SeleniumFunctionalTestHeroku(TestCase):
#   def setUp(self):
#     chrome_options = Options()
#         # remove four lines below to test locally
#     chrome_options.add_argument('--dns-prefetch-disable')
#     chrome_options.add_argument('--no-sandbox')
#     chrome_options.add_argument('--headless')
#     chrome_options.add_argument('disable-gpu')
#     self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#     super(SeleniumFunctionalTestHeroku, self).setUp()


#   def tearDown(self):
#     self.selenium.quit()
#     super(SeleniumFunctionalTestHeroku, self).tearDown()
#   def test_color_jumbotron_heroku(self):
#     browser = self.selenium
#     browser.get("https://ppw-sudah-gila.herokuapp.com/")
#     jumbo = browser.find_element_by_class_name('jumbotron')
#     jumbo_color = jumbo.value_of_css_property("background-color")
#     self.assertEquals("rgba(0, 0, 0, 0.5)", jumbo_color);
  
#   def test_style_h2_heroku(self):
#     browser = self.selenium
#     browser.get("https://ppw-sudah-gila.herokuapp.com/")
#     badan = browser.find_element_by_tag_name("body")
#     gambar_badan = badan.value_of_css_property("background-image")
#     self.assertEquals("url("+ "\"" +"https://hdqwalls.com/download/calm-ocean-water-5k-jd-2048x1152.jpg"+ "\"" +")", gambar_badan);

#   def test_header_utama_heroku(self):
#     browser = self.selenium
#     browser.get("https://ppw-sudah-gila.herokuapp.com/")
#     self.assertTrue(browser.find_elements_by_css_selector('body > h1')[0].is_displayed())

#   def test_random_tag_heroku(self):
#     browser = self.selenium
#     browser.get("https://ppw-sudah-gila.herokuapp.com/")
#     self.assertFalse(browser.find_elements_by_css_selector('div.row > div.col-6 > div') == None) 