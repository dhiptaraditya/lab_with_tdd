from django.urls import re_path
from beranda import views
# from .views import index

urlpatterns = [
  re_path(r'^bukus/(?P<search>.*)$', views.bukus),
  # re_path(r'^bukus/(?P<search>[0-9, "a"-"z","A"-"Z"]+)/$', views.bukus),
  # re_path(r'^bukus/<search>/', views.bukus),
  # re_path(r'^bukus', views.bukus),
  re_path(r'^profile', views.profile),  
  re_path(r'^beranda', views.index),
  re_path(r'^buku', views.buku),
  re_path(r'^', views.index),
]
