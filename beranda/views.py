from django.shortcuts import render
from .models import Status
from django.http import HttpResponseRedirect, JsonResponse
from .forms import StatusForms
import urllib.request, json 
import requests 

# Create your views here.
def index(request):
  tmp = Status.objects.all()
  # resp = {}
  if request.method == 'POST':
    form =  StatusForms(request.POST)
    if form.is_valid():
      form.save()
      return HttpResponseRedirect('/')
  else:
    form = StatusForms()
  return render(request, 'home.html', {'form':form, 'data':tmp})


def bukus(request, search):
  print("HALO") 
  url = "https://www.googleapis.com/books/v1/volumes?q=" + search
  datas = requests.get(url)
  datas = datas.json()
  items = datas['items']
  # ctr = 5
  for item in items:
    item['favo'] = False
    # if ctr:
    #   ctr = ctr-1
    # else:
    #   item['favo'] = True
    item = item
  datas = []
  for item in items:
    tmp = {
      'title' : item['volumeInfo']['title'],
      'preview' :  item['volumeInfo']['imageLinks']['thumbnail'],
      'author' : item['volumeInfo']['authors'][0],
      'is_favo' : item['favo']
    }
    # print(item['volumeInfo']['authors'][0])
    datas.append(tmp)
  # print(datas)
  return JsonResponse({'datas':datas})

def buku(request):
  return render(request,'listbuku.html')


def profile(request):
  return render(request, 'profil.html')