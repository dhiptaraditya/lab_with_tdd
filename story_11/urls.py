from django.urls import re_path, path, include
from django.conf import settings
from django.contrib.auth.views import LogoutView
from story_11 import views


# from .views import index

urlpatterns = [
  # re_path(r'^bukus/(?P<search>.*)$', views.bukus),
  # re_path(r'^profile', views.profile),  
  # re_path(r'^beranda', views.index),
  # re_path(r'^buku', views.buku),
  path('', include('social_django.urls', namespace='social')),
  re_path(r'^logout/', LogoutView.as_view(), {'next_page': settings.LOGOUT_REDIRECT_URL}, name='logout'),
  re_path(r'^', views.index),
]
