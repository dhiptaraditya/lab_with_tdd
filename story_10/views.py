from django.shortcuts import render
from .models import People
import json
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, JsonResponse

# Create your views here.

def index(request):
  return render(request, 'index.html')

def showdb(request):
  data = list(People.objects.values())
  datas = {
    'data' : data
  }
  return JsonResponse(datas)

def registrate(request):
  if(request.method == "POST"):
    data = json.loads(request.body)
    person = People(password = data['pass'], name = data['name'], email = data['email'])
    person.save()
    print(data)
  return HttpResponse("<h2>Succed</h2>")

@csrf_exempt
def deletion(request):
  if(request.method == "POST"):
    data = json.loads(request.body)
    people = People.objects.get(password = data['pass'])
    print("asdd")
    print(people)
    people.delete()
  return HttpResponse("<h2>Succed</h2>")
